import { LoginService } from './../login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email = ''
  password = ''

  constructor(private router:Router,
    private loginService:LoginService) { }

  ngOnInit(): void {
  }


  onLogin() {
    if (this.email.length == 0) {
      alert('require email')
    } else if (this.password.length == 0) {
      alert("require password")
    } else {
      this.loginService.Login(this.email, this.password)
        .subscribe(response => {
          if (response['status'] == 'success') {

            const data = response['data']
            console.log(data);
            alert(` WELCOME `)
            this.router.navigate(['/dashboard'])
          } else {
            console.log(response['error']);
            alert('Invalid email or password')

          }

        })
    }
  }





}
