import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url='http://localhost:3000/employee'

  constructor(private httpClient : HttpClient) { }


  Login(email:string,password : string){
 
    const body={
      email:email,
      password:password
    }
  
     return this.httpClient.post(this.url+"/signin",body)

  }

}
