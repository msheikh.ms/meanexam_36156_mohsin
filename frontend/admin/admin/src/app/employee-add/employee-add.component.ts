import { EmployeeService } from './../employee.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {
  employee = null
  empName = ''
  email = ''
  address = ''
  mobile = ''

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    const id = this.activatedRoute.snapshot.queryParams['id']
    if (id) {
      this.employeeService.getParticularEmployee(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const empolyees = response['data']
            if (empolyees.length > 0) {
              this.employee = empolyees[0]
              this.empName = this.employee['empName']
              this.email = this.employee['email']
              this.mobile = this.employee['mobile']
              this.address = this.employee['address']
            }
          }
        })
    }





  }



  addEmploye() {
    this.employeeService
      .insertEmployee(this.empName, this.email, this.mobile,  this.address)
      .subscribe(response => {
        if (response['status'] == 'success') {
          alert('employee successfully added')
          this.router.navigate(['/employee'])
        }
        else {
          console.log(response['error'])
        }
      })
  }





}
