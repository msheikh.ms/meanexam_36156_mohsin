import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  url = "http://localhost:3000/employee"

  constructor(private httpClient: HttpClient,
    private router: Router) { }



  getEmployees() {
    return this.httpClient.get(this.url)
  }



  getParticularEmployee(id: number) {
    return this.httpClient.get(this.url + '/details/' + id)
  }


  insertEmployee(empName: string,  email: string, mobile: string, address: string) {
    const httpOption={
      headers : new HttpHeaders({
        token : sessionStorage['token']
      })
    }
   
     const body = {
       empName: empName,
       email: email,
       mobile: mobile,
       address: address
 
     }
     return this.httpClient.post(this.url + '/add', body,httpOption)
   }





}
