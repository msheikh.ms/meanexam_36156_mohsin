import { EmployeeService } from './../employee.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  employees = []
  empName = ''
  email = ''
  mobile = ''
  constructor(
    private router: Router,
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    this.loadEmployees()
  }

  loadEmployees(){
    this.employeeService.getEmployees()
    .subscribe(response=>{
      if(response['status']=='success'){
         this.employees=response['data']
         console.log(this.employees);
         
      }else{
        console.log(response['error']);
        
      }
    })
  }




  onAdd(){
    this.router.navigate(['/employee-add'])
  
  }


  onEdit(employee){
    this.router.navigate(['/employee-add'],{queryParams:{id:employee['id']}})
  }




}


