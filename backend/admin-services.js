const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
const cors = require('cors')

const adminRouter = require('./admin/routes/admin')


app.use(cors('*'))


app.use('/admin',adminRouter)



app.listen('3000', '0.0.0.0', () => {
    console.log('employee port lisenten at 3000');
})