const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
const cors = require('cors')

const employeeRouter = require('./employee/routes/employee')


app.use(cors('*'))


app.use('/employee',employeeRouter)



app.listen('3000', '0.0.0.0', () => {
    console.log('employee port lisenten at 3000');
})