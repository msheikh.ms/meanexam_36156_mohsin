const express = require('express')
const router = express.Router()

const db = require('../../db')

const utils = require('../../utils')



// =================================================================================================
//                               GET
// =================================================================================================

router.get('/', (request, response) => {
    const statement = `select * from employee`
    db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
})





module.exports = router